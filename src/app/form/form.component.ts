import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularMyDatePickerDirective, 
  IAngularMyDpOptions, 
  IMyDateModel, HeaderAction } from 'angular-mydatepicker';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @ViewChild('dp') mydp: AngularMyDatePickerDirective;
  myDatePickerOptions: IAngularMyDpOptions = {
    // options here...
  }
  model: IMyDateModel = null
  // set default locale 'ja' (Japanese)
  locale: string = 'ja';
  formValues:any = [];
  firstName: string= '';
  lastName: string= '';
  screenName: string= '';
  constructor() { }

  ngOnInit() {
  }
  onSubmit(){
    this.formValues.firstName=this.firstName;
    this.formValues.lasttName=this.lastName;
    this.formValues.screenName=this.screenName;
    this.formValues.date=this.model;
  }
  changeLocaleDynamically(): void {
    this.locale = 'ru';
  }
}
