import { AfterContentInit, Component, DoCheck, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, OnDestroy{
  @Input() simpleInput: string;
  title = 'app';
  constructor(){
    console.log('I am on constructor');
  }
  ngOnChanges(changes : SimpleChanges){
    for (let propertyName in changes){
      let change = changes[propertyName];
      let current = JSON.stringify(change.currentValue);
      let previous = JSON.stringify(change.previousValue);
      console.log(propertyName + ': currentValue = ' +current + ', previousValue =' + previous);
    }
  }
  ngOnInit(){
    console.log('I am on init');
    this.title = 'app1';
    this.simpleInput='appp'
  }

  ngDoCheck(){
    console.log('I am a do check');
  }

  ngAfterContentInit(){
    console.log('I am a ngAfterContentInit');
  }

  ngOnDestroy(){
    console.log('I am on destroy');
    this.title='';
  }
}
