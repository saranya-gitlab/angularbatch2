import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { FormComponent } from './form/form.component';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { ShowFormComponent } from './show-form/show-form.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    ShowFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularMyDatePickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
