import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-form',
  templateUrl: './show-form.component.html',
  styleUrls: ['./show-form.component.css']
})
export class ShowFormComponent implements OnInit {
  @Input() formValues;
  constructor() { }

  ngOnInit() {
  }

}
